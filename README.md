### Hoe start ik deze app?

Stap 0:
Maak een mapje aan dat logs heet in deze projectfolder. Daar komen de logs in terecht.
Hij staat op het hoogste niveau, op /django-todo/logs


#### Windows:
1. Ga in de terminal naar de map waar deze code staat. Dus in de map `django-todo`.
2. (dit hoeft alleen maar de eerste keer!!) Maak een virtual environment aan: `python -m venv .venv`. De virtual environment komt dan in een geheime map `.venv` terecht.
3. Activeer de environment: `.\.venv\Scripts\activate`.
4. Installeer de requirements: `pip install -r requirements.txt`: alle requirements uit deze file worden dan geïnstalleerd.
5. Deze app gebruikt een eigen database. Je maakt deze aan met het commando: `python manage.py migrate`. 
6. Je kunt de Django website starten door het commando `python manage.py runserver`; dan is de webpagina bereikbaar op localhost:8000. Let op dat je geen andere apps hebt draaien op dezelfde URL!

#### Mac / Linux:
1. Ga in de terminal naar de map waar deze code staat. Dus in de map `django-todo`.
2. (dit hoeft alleen maar de eerste keer!!) Maak een virtual environment aan: `python -m venv .venv`. De virtual environment komt dan in een geheime map `.venv` terecht.
3. Activeer de environment: `source .venv\bin\activate`.
4. Installeer de requirements: `pip install -r requirements.txt`: alle requirements uit deze file worden dan geïnstalleerd.
5. Deze app gebruikt een eigen database. Je maakt deze aan met het commando: `python manage.py migrate`. 
6. Je kunt de Django website starten door het commando `python manage.py runserver`; dan is de webpagina bereikbaar op localhost:8000. Let op dat je geen andere apps hebt draaien op dezelfde URL!

#### Git bash terminal
Volgens mij kun je in een Git bash terminal de Mac / Linux commando's volgen.

#### Pycharm
1. Open het project in Pycharm. Bij het openen krijg je een pop-up over het instellen van een virtual environment. Check even of de environment .venv heet en in de juiste map staat (onder django-todo).
2. Als je een terminal opent in Pycharm, dan is automatisch de environment geactiveerd.
3. Deze app gebruikt een eigen database. Je maakt deze aan met het commando (in die terminal): `python manage.py migrate`. 
4. Je kunt de Django website starten door het commando `python manage.py runserver`; dan is de webpagina bereikbaar op localhost:8000. Let op dat je geen andere apps hebt draaien op dezelfde URL!


Schematische weergave van een Django applicatie door top-student Jeroen:
![alt text](plaatJeroenR.jpeg?raw=True)

