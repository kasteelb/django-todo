### Deel 0: eigen branch en intro
Clone het project en maak je eigen branch aan. Vraag toestemming om naar dit project te mogen pushen (knop op Gitlab). Verander een klein beetje code ergens, en push je branch.

Ik zou het fijn vinden als jullie regelmatig een push doen naar de server van jullie wijzigingen: dan kan ik een beetje zien hoe het jullie vergaat!

Er zijn een aantal opdrachten (1-3), waarin je oefent met wat begrippen die al in deze app zitten.

Deel_x is een wat grotere opdracht. Die hoeft deze week nog niet af, maar je kunt er wel alvast aan beginnen.

Dit is een test om te checken of het werkt