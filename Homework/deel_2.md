### Deel 2: Eigen database uitbreiden
In models.py coderen we de structuur van onze database. Iedere class (van het type models.Model), definieert een database tabel.

De velden die we in zo'n model plaatsen, zijn de kolommen in die tabel.

1. Maak een nieuwe kolom in de tabel List: points. Deze kolom moet integers gaan bevatten --> IntegerField. Geef ook een default aan.

2. (stop eerst de server) We hebben nu models.py veranderd, maar onze database verandert niet automatisch mee. Dat moet door middel van een migratie.
Voor in de terminal het commando `python manage.py makemigrations` uit.
Kijk dan in todo_list/migrations naar de (door jou) gegenereerde code!
   (waarschijnlijk heet hij 0002_...)
   
3. Django kan deze migratie-file gebruiken om de database aan te passen. Dat doe je met het commando `python manage.py migrate`.