from django.shortcuts import render, redirect
from .models import List
from .forms import ListForm
from django.contrib import messages

import logging

# This creates a logger called 'logger-example':
logger = logging.getLogger('logger-example')
# In todo_app.settings.py we refer to this name: we have set it up so that logs from logger with this name
# Will be written to /logs/debug.log


def home(request):
	# This function shows logging using the standard Python logging module.
	if request.method == 'POST':
		logger.info(f'Someone ({request.user}) wants to post a new item!')
		form = ListForm(request.POST or None)

		if form.is_valid():
			form.save()
			all_items = List.objects.all()
			messages.success(request, 'Item Has Been Added To List!')
			return render(request, 'home.html', {'all_items': all_items})
		else:
			# Todo: create error page
			return None

	else:
		all_items = List.objects.all()
		return render(request, 'home.html', {'all_items': all_items})


def about(request):
	context = {'first_name': 'Gerben', 'last_name': 'Meijer'}
	return render(request, 'about.html', context)


def delete(request, list_id):
	if request.method == 'GET':
		logger.info(f'Someone ({request.user}) deletes item!')
		form = ListForm(request.GET or None)
		item = List.objects.get(pk=list_id)
		item.delete()
		messages.success(request, 'Item Has Been Deleted!')
		print(item, "deleted")
		return redirect('home')
	else:
		return redirect('home')

def cross_off(request, list_id):
	item = List.objects.get(pk=list_id)
	item.completed = True
	item.save()
	return redirect('home')	


def uncross(request, list_id):
	item = List.objects.get(pk=list_id)
	item.completed = False
	item.save()
	return redirect('home')	


def edit(request, list_id):
	# This function contains logging in the form of print statements. They are printed in the django server terminal
	# (at the same spot where you wrote 'python manage.py runserver').
	if request.method == 'POST':
		print('body\n', request.body, '\n')
		item = List.objects.get(pk=list_id)

		form = ListForm(request.POST or None, instance=item)
		print('form\n', form.__dict__.keys(), '\n')

		if form.is_valid():
			print('cleaned', form.cleaned_data)
			form.save()
			messages.success(request, 'Item Has Been Edited!')
			return redirect('home')

	else:
		item = List.objects.get(pk=list_id)
		return render(request, 'edit.html', {'item': item})

