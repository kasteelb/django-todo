from django.db import models


class List(models.Model):
	item = models.CharField(max_length=200)
	completed = models.BooleanField(default=False)
	point = models.IntegerField(default=0)


	def __str__(self):
		return self.item + ' | ' + str(self.completed) + '|' + str(self.id) + '|' + str(self.point)


class Plant(models.Model):
	item = models.CharField(max_length=200)
	completed = models.BooleanField(default=False)
	number = models.IntegerField(default=0)
	date = models.DateTimeField(default=False)

	def __str__(self):
		return self.item + ' | ' + str(self.completed) + '|' + str(self.id) + '|' + str(self.number) + '|' + str(self.date)
